// import React, { useState } from "react";
import React from "react";

// import Header from './components/Header';
import './global.css';

// import Logon from './pages/Logon';
import Routes from './routes';

function App() {
    // let [counter, setCounter] = useState(0);

    // function increment() {
    //     setCounter(counter + 1);
    // }

    return (
        <div>
            {/* <Header>Contador: {counter}</Header> */}
            <Routes />

            {/* <button onClick={increment}>Incrementar</button> */}
        </div>
    );
}

export default App;
